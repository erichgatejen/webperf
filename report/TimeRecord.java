import java.util.*;

public class TimeRecord {

   Vector  list;

   public  int time;

   int  low, high;

   public TimeRecord(int  timeLow,  int timeHigh) {
	
	low  = timeLow;
	high = timeHigh;
	list = new Vector();

	// System.out.println("Build a TR: l = " + low + " h = " + high);

   }

   public boolean isIn(TimeEntry  entry) {

	if ((entry.time > high)||(entry.time < low)) {
         return false;   
 	}
	return true;
   }

   public boolean put(TimeEntry  entry) {
   
	if (!this.isIn(entry)) return false;
	
	list.addElement(entry);
	return true;
   }

   public String report() {

	TimeEntry	e;

	int count = 0, aLatency = 0, aSize = 0, size = 0, latency = 0, 
	    aGood = 0, aBad = 0, aNet = 0, time = 0; 

	StringBuffer  output = new StringBuffer(" " + low + " -> " + high + " : ");
	
	while (list.size() > 0) {
	   e = (TimeEntry) list.elementAt(0);         
         list.removeElementAt(0);
	     
	   aLatency += e.latency;
	   aSize    += e.size;

	   count++;
	
	   switch(e.result) {
		case 1 : aGood++; break;
		case 2 : aBad++;	break;
		case 3 : aNet++;  break;
		default : 
			System.out.println("Software Detected Fault:  TimeRecord : Unknown result - " + e.result);
	   }
	
	} // end while

	if (count > 0 )  { 
		size = aSize / count;
		latency	= aLatency / count;
	} else {
		size    = 0;
		latency = 0;
	}

	output.append("c=" + count + " l=" + latency + " s=" + size + " GBN=" 
			  + aGood + "," + aBad + "," + aNet + " \n");

	return output.toString();

   }
}

