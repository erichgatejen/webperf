import java.util.*;
import java.io.*;

public class PerfRedux {
	
    static TimeRecord   currentRecord;

    static int 	 	  gran;  
    static FileWriter 	  out;

    public static void main(String[] args) {

	 String	currentString;   
	 String	finalReportString;
 
       String   target;
       String   destination;


       try {
	    target 		= (String) args[0];
	    destination	= (String) args[1];
	    gran   = Integer.parseInt(args[2]);
	 } catch (Exception e) {
	    System.out.println("ERROR! Malformed command line arguements");
	    return;	
	 }	 

	 // Build the first time record
	 currentRecord = new TimeRecord(0, gran-1);

	 // Open files and start read loop
	 try { 

	    out     = new FileWriter(destination);
	
	    FileReader 	  theFile = new FileReader(target);
	    BufferedReader  in      = new BufferedReader(theFile);

	    currentString = in.readLine();
	    while( currentString != null ) {
	       
		 handleLine(currentString); 

	       currentString = in.readLine();
	    }

	    if (currentRecord != null) {

		 // code to clear out last time record
 		finalReportString = currentRecord.report();
		out.write(finalReportString, 0, finalReportString.length());
  
	    }

	    in.close();
	    out.close();
      
	} catch (FileNotFoundException e) {  
	      System.out.println("ERROR! Could not find FILE: " + target);
	} catch (IOException e) { 
		System.out.println("ERROR! General IO Error");
	}

   }  // end main()

   private static void  handleLine(String  line) {

	StringBuffer  timeString = new StringBuffer();
	int 		  rover = 0;
	char		  tempChar;

	System.out.println("HANDLE : " + line);

	// if we bust a bounds here anywhere, then the line is not a valid put.  
	// catch the exception and move on.
	try  {

         // Discard if does not start with time
	   if ( !Character.isDigit(line.charAt(0)) ) return;

	   // Peal off important elements.  Return if any indicate this is not a put record.
	   tempChar = line.charAt(rover);
	   while (Character.isDigit(tempChar)) {
	      timeString.append(tempChar);
	      rover++;
		tempChar = line.charAt(rover);
	   }

	   // Jump 3 chars.  If not a P, then ditch.
	   rover += 3;
	   if (line.charAt(rover) != 'P') {
		System.out.println("DROP");
		return;
	   };

	   // Ok.  I think we have a put line.  Go ahead and start building the entry
	   TimeEntry te = new TimeEntry();
	   te.time = Integer.parseInt(timeString.toString());

	   // What was the result code?  Find a digit
	   while ( !Character.isDigit(line.charAt(rover)) ) { rover++; }
	
	   te.result = Character.digit(line.charAt(rover), 10);
	   rover++;

	   // Find the latency
	   while ( !Character.isDigit(line.charAt(rover)) ) { rover++; }
	   StringBuffer  latencyString = new StringBuffer();
	   tempChar = line.charAt(rover);
	   while (Character.isDigit(tempChar)) {
	      latencyString.append(tempChar);
	      rover++;
		tempChar = line.charAt(rover);
	   }
	   te.latency = Integer.parseInt(latencyString.toString());

	   // Find the size
	   while ( !Character.isDigit(line.charAt(rover)) ) { rover++; }

	   StringBuffer  sizeString = new StringBuffer();
	   tempChar = line.charAt(rover);
	
	   while (Character.isDigit(tempChar)) {
	      sizeString.append(tempChar);
	      rover++;
		try { 
		   tempChar = line.charAt(rover);  
		} catch (StringIndexOutOfBoundsException es) {
		   // This just means there is no white space at the end of the line.  technically,
		   // it is ok.
		   tempChar = 'x';  // something that ISNT a digit  :-)
		}
	   }
		
	   te.size = Integer.parseInt(sizeString.toString());
  
	   // We have a whole record.  Try to put it in somewhere
	   if (currentRecord.isIn(te)) {
		System.out.println("Push");		
		currentRecord.put(te);
		
	   } else {
		// Not in that time zone.  report the current and build a new one
		System.out.println("NEW");		

		String outString = currentRecord.report();
		out.write(outString, 0, outString.length());

		int  newBase  = te.time - (te.time % gran);
		currentRecord = new TimeRecord(newBase, (newBase + (gran-1)) );

		if (!currentRecord.put(te)) {
			System.out.println("Software detected fault:  couldnt stuff a brand-new TimeRecord"); 
		}
	   }
  	   
	} catch (Exception e) { };

   } // end handeLine()

}

