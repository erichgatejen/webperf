// Fetch a URL.  Record the time.  Return a result object.

import java.io.*;
import java.net.*;
import java.util.*;

public class FetchResult {
   
   public long			time;
   public int		      status;
   public int			size;
   public String			httpStatus;
  
   public static final int	FR_OK  		= 1;
   public static final int	FR_CONNECT_FAILED = 2;
   public static final int	FR_READ_FAILED    = 3;
   public static final int    FR_UNKNOWN_ERROR  = 99;

   public FetchResult() {
	time = 0;
	size = 0;
   }
   
   public void start() {

	Date d = new Date();
	time = d.getTime();
   }

   public void end() {
	Date d = new Date();
	time = d.getTime() - time;
   }
}
