import java.util.*;
import java.io.*;

public class Logging {
   
   ObjectMonitor   myLock; 
   long		 start; 
   long		 pause;
   FileWriter      out;
   boolean		 isOpen;
   static String	 currentLogName;

   Logging() {
	myLock    = new ObjectMonitor();
	start	    = 0;
	isOpen    = false;

   }

   public void done() {
	if (isOpen == false)  {
		System.out.println("LOGGING: Attepted to done() a log that was already closed.");
		return;
	}
	//System.out.println("LOGGING: DONE");
	try {
	   out.close();
	} catch (Exception e) {
	   System.out.println("LOGGING: Failed to close log file in done().");
	}
	isOpen = false;
   }    

   public void post(String entry) {

	if (isOpen == false) return;
	//System.out.println("LOGGING: POST");
      
	long	  myTime;
	String  postString;

	myLock.lock(true);

   	Date	 time = new Date(); 
	myTime      = getTiming();
	try {
		postString = myTime + " : " + entry + "\n";
		out.write(postString, 0, postString.length());

	} catch (Exception e) {  
      }

      myLock.lock(false);
   }     

   public String getLog() {
   	  
	int      chars	 = 1;
	char     buf[] 	 = new char[2048];
 	FileReader   lf    = null;

	StringBuffer lb = new StringBuffer();

	if (isOpen == true)  {
		System.out.println("LOGGING: Tried to getlog on an open log.");
		done();
	}

	try {
 
	   // Copy the file to the report buffer
	   System.out.println("LOGGING: trying to open " + currentLogName);
	   lf = new FileReader(currentLogName);
	   System.out.println("LOGGING: Opened log file.");
         while (chars > 0) {
            chars = lf.read(buf,0,2048);
		lb.append(buf,0,chars);
         }

	} catch (Exception e) {
	   System.out.println("LOGGING: Exception failure while trying to getLog().  It should be OK.");
	} finally {
		try {
	         lf.close();
		} catch (Exception e) { }
	}

	return lb.toString();
   }


   public void clear() {

	if (isOpen == true) {
	   System.out.println("LOGGING: Attempted to clear() log before closing old log.  Closing old log.");
	   done();
	}

      myLock.lock(true);
	isOpen = false;
	Date	 logTime = new Date();
	try { 
	   currentLogName = new String("log-" + logTime.getTime() + ".s");
	   out = new FileWriter(currentLogName); 
	   //System.out.println("LOGGING: OPEN " + "log-" + logTime.getTime() + ".s");
	   isOpen = true;
	   myLock.lock(false);

	} catch (Exception e) { myLock.lock(false);
					System.out.println("LOGGING: Failed to open log file.");
					//throw(e); 
				    }  // toss this one at the wind.  it is a bad, bad thing
   }

   public void startTiming() {

	Date	 time = new Date(); 
	start = time.getTime();	
   }

   public void stopTiming() {
	start = 0;
   }
   
   public void pauseTiming() {
   	Date	 time = new Date(); 
	pause = time.getTime();	
   }

   public void resumeTiming() {
   	Date	 time = new Date(); 
	start = time.getTime() - (pause-start);
   }


   public long getTiming() {
   	Date	 time = new Date();
	long  current;
	if (start > 0)
	   current = time.getTime() - start;
	else
  	   current = 0;
	return current;
   }
}
