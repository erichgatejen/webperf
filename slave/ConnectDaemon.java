public abstract class ConnectDaemon extends Thread {
   
   public ConnectDaemon(boolean demon) {
      setDaemon(demon);
   }

   public void run() {
      HTTPDaemon httpd;
      Connection client;

      System.out.println("ConnectDaemon: Im up.");  

      if (initialize()) {                         
         httpd = new HTTPDaemon(8080);
         httpd.setUseHostName(false);
         System.out.println("Exposed: " + httpd.url());


         while ((client = httpd.accept()) != null) {         
            handleConnection(client);
         }                    
      }
   }

   protected boolean initialize() {
      return true;
   }
      
   protected abstract void handleConnection(Connection client);
}     

