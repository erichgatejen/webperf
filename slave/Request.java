// PUT  = Put the URL
// WAIT = Sleep the chute for X milliseconds

import java.util.*;

public class Request extends Object {
  
   public static final int reqNULL                 = 0;
   public static final int reqPUT                  = 1;
   public static final int reqWAIT	               = 2;

   public int  	type; 
   public int    	intParam;
   public String 	text;

   public Request() {

   }

   public void set(int  typeP, int   intP,  String  textP) {
      
	type		= type;
	intParam	= intP;
	text		= textP;
   }

   public int getType() {
      return type;
   }

}

