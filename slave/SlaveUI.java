import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class SlaveUI implements ActionListener {
 
   // UI Elements
   Frame	   f;
   TextArea    screenLog;
   TextField   currentScriptName;
   TextField   currentTarget;
   TextField   scriptTime;
   Button      buttonAllow;
   Button	   buttonDisallow;
   Panel	   panelButton;
   Panel 	   panelInfo;
 
   SlaveUI(Frame f) {
   
	initialize(f);
   }
   
   private void initialize(Frame f) {  
	
	this.f = f;
	f.setLayout(new BorderLayout());
	f.setTitle("JF Performance Test Tool - SLAVE");
      
      // Build UI
	screenLog 	      = new TextArea("Started...\n\n");
      currentScriptName = new TextField("!!NO SCRIPT LOADED!!");
      currentTarget     = new TextField("!!NO TARGET SPECIFIED!!");
	scriptTime		= new TextField("0",12);
	panelButton	   = new Panel();
	panelInfo	   = new Panel();
	buttonAllow    = new Button("Allow");
	buttonDisallow = new Button("Disallow");

	currentScriptName.setEditable(false);
	screenLog.setEditable(false);
	scriptTime.setEditable(false);
	currentTarget.setEditable(false);

	// build top panal
	panelInfo.setLayout(new BorderLayout());
	panelInfo.add(currentScriptName, "North");
	panelInfo.add(currentTarget, "Center");
	panelInfo.add(scriptTime, "East");
     	f.add(panelInfo, "North");

	// build center log area
	f.add(screenLog, "Center");

	// build bottom button panal
	panelButton.setLayout(new FlowLayout(FlowLayout.LEFT));
	panelButton.add(buttonAllow);
	panelButton.add(buttonDisallow);
	buttonAllow.addActionListener(this);
	buttonDisallow.addActionListener(this);	
	f.add(panelButton, "South");
	
	f.setSize(550,580);
	f.show();
	      
   }
   
   public void actionPerformed(ActionEvent  e) {
 
	if(e.getSource() == buttonAllow) {
	   screenLog.append("Set ALLOW.\n");

	}
	else if (e.getSource() == buttonDisallow) {
	   screenLog.append("Set DISALLOW.\n");

	}
   }

   public void putScreenLog(String entry) {
	screenLog.append(entry + "\n");
   }

   public void setTarget(String name) {
	currentTarget.setText(name);
   }

   public void setScriptTime(String name) {
	scriptTime.setText(name);
   }

   public void setScriptName(String name) {
	currentScriptName.setText(name);
   }

}
