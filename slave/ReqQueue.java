import java.util.*;

public class ReqQueue {
   
   ObjectMonitor   myLock; 
   Vector		 queue;  
   int 		 queueSize;

   // Use default constructor

   public void init() {
	myLock    = new ObjectMonitor();
      queue     = new Vector();
      queueSize = 0;
   }

   public Request get() {
      Request r = null;
      // blocks until a message arrives
      myLock.lock(true);
      if (!queue.isEmpty()) {
         r = (Request) queue.elementAt(0);         
         queue.removeElementAt(0);
	   queueSize--;
      }
      myLock.lock(false);
      return r;
   }
   
   public void put(Request r) {
      myLock.lock(true);
      queue.addElement(r);
	queueSize++;
      myLock.lock(false);
   }
   
   public int size() {
      return queueSize;
   }
        
}
