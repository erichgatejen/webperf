// 
//  Chutes start from 1.  Chute #0 means "dont care"
//  Users start from 1.  User # 0 is a NON-USER

import java.util.*;

public class ScriptItem {

   public final static int	tokenNone 	 = 0;
   public final static int	tokenName    = 1;
   public final static int	tokenPut   	 = 2;
   public final static int	tokenWait  	 = 3;
   public final static int	tokenEndLoop = 4;
   public final static int	tokenLoop  	 = 5;
   public final static int	tokenEnd     = 6;
   public final static int	tokenSync    = 7;
   public final static int	tokenIndexPut  = 8;
   public final static int	tokenIndexSet  = 9;
   public final static int	tokenIndexInc  = 10;
   public final static int	tokenChute     = 11;
   public final static int	tokenAssign    = 12;
   public final static int	tokenIndexWait = 13;

   public final static int	tokenBAD       = 99;

   public final static int	maxLoop        = 999999;

   public final static char	tokenDelimit	= '[';
   public final static char	numberDelimit	= ',';
   public final static char	commentChar		= '#';
   public final static char	idChar		= '*';

   public final static String  tokenTextName     = "name";   // name-supportedusers-text
   public final static String  tokenTextChute	 = "chutes";  // chutes-num of chutes to use-nil
   public final static String  tokenTextAssign	 = "assn";	 // assn-user-chute

   public final static String  tokenTextPut      = "put";	 // put-<useridx>-text
   public final static String  tokenTextIndexPut = "iput";	 // iput-nil-text
   public final static String  tokenTextIndexSet = "set";    // set-value-nil
   public final static String  tokenTextIndexInc = "inc";    // inc-nil-nil
   public final static String  tokenTextIndexWait = "iwait";  // iwait-value-nil
   public final static String  tokenTextWait     = "wait";    // wait-value-<trail an ID>
   public final static String  tokenTextEndLoop  = "endloop"; // endloop-nil-label
   public final static String  tokenTextLoop     = "loop";	  // loop-count-label
   public final static String  tokenTextEnd      = "end";     // end-nil-nil
   public final static String  tokenTextSync     = "sync";	  // sync-nil-nil

   public int	token;
   public int	count;	// used as loop count and jump location
   public int 	id;		// assign to a specific user/chute
   public String  text; 

   ScriptItem() {
	token = tokenNone;
   }

   public int is() {
	return token;
   }
   
}
