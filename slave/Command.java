import java.util.*;
import java.io.*;

public class Command extends Object {
  
   public static final int cmdNULL                 = 0;
   public static final int cmdSTOP                 = 1;
   public static final int cmdNEW	               = 2;
   public static final int cmdPAUSE		         = 3;
   public static final int cmdTARGET	         = 4;
   public static final int cmdGETLOG		   = 5;
   public static final int cmdGO			   = 6;
   public static final int cmdPING			   = 7;

   public static String cmdTextSTOP		   	   = "stop";
   public static String cmdTextNEW			   = "new";    // new-userstart-nil
   public static String cmdTextPAUSE		   = "pause";
   public static String cmdTextTARGET		   = "target";
   public static String cmdTextGETLOG		   = "getlog";
   public static String cmdTextGO			   = "go";
   public static String cmdTextPING			   = "ping"; 

   private int  		command; 
   private int    	intParam;
   private long 		longParam;
   private String 	stringParam;
   private Connection	connect;
   private String		protocol;

   public Command() {

   }

   public void setCommand(int  theCommand, int   intP,  long   longP,  String  stringP,
			   Connection  inConnect, String  inProtocol) {
      
	command 	= theCommand;
	intParam	= intP;
	longParam   = longP;
      stringParam = stringP;		// lets just hold the reference for now.
	connect	= inConnect;	// let us respond to a request.
      protocol	= inProtocol;
   }

   public int getCommand() {
      return command;
   }

   public int getIntParam() {
      return intParam;
   }

   public long getLongParam() {
      return longParam;
   }
   
   public String getStringParam() {
      return stringParam;
   }

   public void respond(String  content) {

      try {
        connect.println(protocol.trim() + " 200 OK");
        connect.println("Server: JFPerf SLAVE");
        connect.println("Content-Type: text/html");
        connect.println("Content-Length: " + content.length());
        connect.println("Accept-ranges: bytes");
        connect.println("");
        connect.print(content);
        try { Thread.sleep(100); } catch (Exception e) {}
      } catch (IOException e) {
         System.out.println(e);
      }
      
      connect.close();	
   }

}

