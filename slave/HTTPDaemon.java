import java.io.*;
import java.net.*;

class HTTPDaemon {
	
   int port = 80;
   
   ServerSocket serverSocket;
   boolean useNames = true;
    
   public HTTPDaemon(int port) {
      this.port = port;   
      
      try {      
         serverSocket = new ServerSocket(this.port);
      } catch (Exception e) {
         System.out.println("Socket Exception.  message=" + e);
      }
   }

   public void setUseHostName(boolean set) {
      useNames = set;
   }

   public String url() {
      String url;
      
      if (serverSocket != null) {
         try {
            if (useNames) {
               url = "http://" + InetAddress.getLocalHost().getHostName() + port;
            } else {
               url = "http://" + InetAddress.getLocalHost().getHostAddress() + port;
            }
         } catch (UnknownHostException e) {
         }
         return url;
      }
      
      return null;
   }
   
   public Connection accept() {
      try {    
         Socket s = serverSocket.accept();
         
         return new Connection(s);           
      } catch (Exception e) {
         System.out.println("Accept Exception: " + e);
      }
      return null;
   }
}
         

   
   

     
     
