/*
 * decode the URL.  Borrowed code.
 */

import java.net.*;
import java.io.*;

class URLDecoder {
   private URLDecoder() {}

   public static String decode(String s) {
      ByteArrayOutputStream out = new ByteArrayOutputStream(s.length());
      for (int count = 0; count < s.length(); count++) {
          if(s.charAt(count) == '%') {
            count++;          
            int a = Character.digit(s.charAt(count++),16);
            a = a<<4;
            int b = Character.digit(s.charAt(count),16);
            out.write(a+b);         
         } else {
            if (s.charAt(count) == '+')  {
               out.write(' ');
            } else {
               out.write(s.charAt(count));
            }     
         }
      }
   
      return (out.toString());   
   }
}

