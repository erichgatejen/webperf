import java.io.*;
import java.net.*;

public class Connection {
   Socket client;
   DataInputStream is;
   DataOutputStream os;
   
   public Connection(Socket s) throws IOException {
      client = s;
      
      is = new DataInputStream(client.getInputStream());
      os = new DataOutputStream(client.getOutputStream());    
   }
   
   public DataInputStream getInputStream() {
      return is;
   }
   
   public DataOutputStream getOutputStream() {
      return os;
   }
   
   public void close() {
      try {
         os.flush();
         client.close();
      } catch (IOException e) {
         // Dont' care
      }
   }
   
   public void println(String s) throws IOException {
      print(s);
      os.write('\n');
   }
   
   public void print(String s) throws IOException {
      byte bytes[] = s.getBytes();

      os.write(bytes);
   }
}
