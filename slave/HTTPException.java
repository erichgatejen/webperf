public class HTTPException extends RuntimeException {
   public HTTPException() {};
   public HTTPException(String message) {
      super(message);
   }
}
