import java.util.*;

public class CmdQueue {
   
   ObjectMonitor   myLock; 
   Vector		 queue;  
   int 		 queueSize;

   CmdQueue() {
	myLock    = new ObjectMonitor();
      queue     = new Vector();
      queueSize = 0;
   }

   public Command getCommand() {
      Command c = null;
      // blocks until a message arrives
      myLock.lock(true);
      if (!queue.isEmpty()) {
         c = (Command) queue.elementAt(0);         
         queue.removeElementAt(0);
	   queueSize--;
      }
      myLock.lock(false);
      return c;
   }
   
   public void putCommand(Command c) {
      myLock.lock(true);
      queue.addElement(c);
	queueSize++;
      myLock.lock(false);
   }
   
   public boolean isCommand() {
      if (queueSize > 0) return true;
	else return false;
   }
        
}
