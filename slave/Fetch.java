// Fetch a URL.  Record the time.  Return a result object.

import java.io.*;
import java.net.*;

public class Fetch {
   
   static final int	BUFFER_SIZE	= 8192;

   public Fetch() {

   }

   InputStream in = null;

   public FetchResult get(URL  url) {

      int     bytes	 = 1;
	byte	  buf[] 	 = new byte[BUFFER_SIZE];
	FetchResult result = new FetchResult();
    
      try {
 	   result.start(); 	
	   
         URLConnection inHTTP = url.openConnection();

	   inHTTP.setUseCaches (false);
         in = inHTTP.getInputStream();                 

	   result.httpStatus = inHTTP.getHeaderField(0);

	   if (result.httpStatus == null) result.httpStatus = new String ("NO VID");

	   // Deplete the instream.
         while (bytes > 0) {
            bytes = in.read(buf);
		result.size += bytes;
         }
         
	   result.status = FetchResult.FR_OK;

      } catch (ConnectException e) {
	   result.status = FetchResult.FR_CONNECT_FAILED;	   

      } catch (IOException e) {
	   result.status = FetchResult.FR_READ_FAILED;	  
 
      } catch (Exception e) {
	   result.status = FetchResult.FR_UNKNOWN_ERROR;

	} finally {
	   try  {
		in.close();
	   } catch (Exception e) { }
      }
            
	result.end();
      return result;

   }

}
