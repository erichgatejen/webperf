import java.io.*;
import java.net.*;
import java.util.*;

public class HTTPIn extends ConnectDaemon {
 
   HTTPDaemon httpd;
   CmdQueue	  cmdQueue;   

   public HTTPIn() {
	super(true);
   }   
 
   public HTTPIn(boolean demon, CmdQueue c) {
      super(demon);
	cmdQueue = c;
   }

   protected boolean initialize() {
      if (!super.initialize()) {
         return false;
      }  
      return true;
   }

   protected void handleConnection(Connection client) {     
		HTTPCmdRead  hr = new HTTPCmdRead(client, cmdQueue);   
		hr.start();
   }
}
