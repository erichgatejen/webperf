import java.io.*;
import java.util.*;

public class HTTPRequest {
   private DataInputStream is;
   String HTTPMethod;
   String HTTPPath;
   String HTTPProtocol;
   String HTTPContent;
   
   Hashtable HTTPHeaders;
         
   public HTTPRequest(DataInputStream is) {
      this.is = is;      
   }

   public void read() throws Exception {
      parseHeaders();
      try {
         if (HTTPMethod.equalsIgnoreCase("POST")) {
            try {
               readContent();
            } catch (Exception e) {
               System.out.println("Exception on read.  message=" + e);
		   throw(e);
            }
         }
      } catch (Exception e) {
	   throw(e); 
      }
   }

   public String method() {
      return HTTPMethod;
   }
   
   public String path() {
      return HTTPPath;
   }
   
   public String protocol() {
      return HTTPProtocol;
   }   

   public String content() {
      return HTTPContent;
   }
      
   private void parseHeaders() throws Exception {   
      String input;
      String request;
      int pos;
      
      try {    
         HTTPHeaders = new Hashtable();
         while ((input = is.readLine()) != null &&  !input.equals("")) {
            // Parse first line separately
            if (HTTPMethod == null) {
               parseRequestLine(input);             
            } else {
               try {
                  parseHeaderLine(input);
               } catch (HTTPException e) {
                  // this isn't fatal, so keep on going
                  System.out.println(e);
               }
            }    
         }
         if (HTTPMethod == null) {
            System.out.println("Http request: No Request Line");
         }
      } catch (Exception e) {
         System.out.println("Failed HTTP request.  message=" + e);
	   throw(e);		
      }

   }
   
   protected void parseRequestLine(String line) throws HTTPException {
      int pos;
      
      line = line.trim();
      pos = line.indexOf(' ');
      if (pos == -1) {
         HTTPException e = new HTTPException("Bad HTTP line.");
         throw(e);
      }

      HTTPMethod = line.substring(0, pos);
      
      line = line.substring(pos).trim();
      pos = line.indexOf(' ');
      
      if (pos == -1) {
       HTTPProtocol = "HTTP/0.9";
       HTTPPath = line;
      } else {
       HTTPPath = line.substring(0, pos);
       HTTPProtocol = line.substring(pos).trim();
      }
   }

   protected void parseHeaderLine(String line) throws HTTPException {
      int pos;

      pos = line.indexOf(':');
      if (pos == -1) {
         HTTPException e = new HTTPException("Bad HTTP line: " + line);
         throw(e);
      }

      String name = line.substring(0, pos);
      String value = line.substring(pos + 2);

      name = name.toLowerCase();
      HTTPHeaders.put(name, value);
   }

   protected String getHeader(String name) {
      String lower = new String(name).toLowerCase();
      lower.toLowerCase();
      return (String) HTTPHeaders.get(lower);
   }
      
   protected void readContent() throws IOException {
      String length = getHeader("Content-Length");
      int bytes;
      
      if (length == null) {
         return;
      }

	int		size;
	byte[]		b = new byte[2048];
	StringBuffer	buffer = new StringBuffer();
	while (true) {
		size     = is.read(b, 0, 2048); 
		if (size == -1) break;
		buffer.append(new String(b, 0, size)); 
		if (is.available() == 0) break; 
	}
      HTTPContent = buffer.toString();
   }
}        
