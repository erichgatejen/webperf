## WEBPERF

This was a very old and very simple web performance test tool.  There really wasn't much available like it when I made
this.  And it worked pretty well.

It has three parts:
- slave - runs an actual test script
- master - manages multiple slaves
- report - taked the output logs and makes a report from them.

(Please excuse the names.  This was literally three decades ago.)

This really has no utility any more, so consider it an archaeological tour of the very early days of Java.

