import java.util.*;

public class MEvent extends Object {
  
   public static final int eventNULL               = 0;
   public static final int eventUI_STOP            = 1;
   public static final int eventUI_START           = 2;
   public static final int eventUI_REFRESH         = 3;
   public static final int eventUI_PAUSE           = 4;
   public static final int eventSLAVE_RESPONSE     = 5;
   public static final int eventTIME	         = 6;

   public static final int rNULL		         = 0;		// Response codes
   public static final int rSUCCESS		         = 1;		
   public static final int rFAIL		         = 2;
   public static final int rIO_ERROR	         = 3;

   public int  		event; 
   public int    		value;
   public int		index;
   public long    	lvalue;
   public Object		item;
   public int		id;

}

// Responses are returned in the following way
//	value  = response code
//	index  = original command
//	lvalue = latency (if applicable)
//    item	 = any requested data (if applicable)
//    id	 = slave id 
