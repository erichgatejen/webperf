import java.awt.*;
import java.awt.event.*;

public class PerfMaster extends Frame implements WindowListener {
    public PerfMaster() {
      addWindowListener(this);
    }

    public void windowOpened(WindowEvent e) {
    }

    public void windowClosing(WindowEvent e) {
      System.exit(0);
    }

    public void windowClosed(WindowEvent e) {
    }

    public void windowIconified(WindowEvent e) {
    }

    public void windowDeiconified(WindowEvent e) {
    }

    public void windowActivated(WindowEvent e) {
    }

    public void windowDeactivated(WindowEvent e) {
    }

    public static void main(String args[]) {
        Frame f = new PerfMaster();
        MasterUI 		theUI 	= new MasterUI(f);
	  Controller	control     = new Controller(theUI); 
	  control.start(); 
    }
}

