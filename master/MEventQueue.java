import java.util.*;

public class MEventQueue {
   
   ObjectMonitor   myLock; 
   Vector		 queue;  
   int 		 queueSize;

   MEventQueue() {
	myLock    = new ObjectMonitor();
      queue     = new Vector();
      queueSize = 0;
   }

   public MEvent get() {
      MEvent c = null;
      // blocks until a message arrives
      myLock.lock(true);
      if (!queue.isEmpty()) {
         c = (MEvent) queue.elementAt(0);         
         queue.removeElementAt(0);
	   queueSize--;
      }
      myLock.lock(false);
      return c;
   }
   
   public void put(MEvent c) {
      myLock.lock(true);
      queue.addElement(c);
	queueSize++;
      myLock.lock(false);
   }
   
   public boolean has() {
      if (queueSize > 0) return true;
	else return false;
   }
        
}
