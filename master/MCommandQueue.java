import java.util.*;

public class MCommandQueue {
   
   ObjectMonitor   myLock; 
   Vector		 queue;  
   int 		 queueSize;

   MCommandQueue() {
	myLock    = new ObjectMonitor();
      queue     = new Vector();
      queueSize = 0;
   }

   public MCommand get() {
      MCommand c = null;
      // blocks until a message arrives
      myLock.lock(true);
      if (!queue.isEmpty()) {
         c = (MCommand) queue.elementAt(0);         
         queue.removeElementAt(0);
	   queueSize--;
      }
      myLock.lock(false);
      return c;
   }
   
   public void put(MCommand c) {
      myLock.lock(true);
      queue.addElement(c);
	queueSize++;
      myLock.lock(false);
   }
   
   public boolean has() {
      if (queueSize > 0) return true;
	else return false;
   }
        
}
