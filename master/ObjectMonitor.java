public class ObjectMonitor extends Object {
   private int locked;
   
   public ObjectMonitor() {
      this(1);
   }
   
   public ObjectMonitor(int lock) {
      locked = lock;
   }
   
   public synchronized void lock(boolean lock) {
      if (lock) {
         while (locked == 0) {
            try {
               wait();
            } catch (InterruptedException e) {
            }
         }
         locked--;
      } else {
         locked++;
         notify();
      }       
   }
}

