import java.util.*;

public class SessionLog {
   
   ObjectMonitor   myLock; 
   StringBuffer	 log;
   long		 start; 
   long		 pause;

   SessionLog() {
	myLock    = new ObjectMonitor();
      log       = new StringBuffer(10000);  //  initial capacity = 10000 chars
	start	    = 0;
   }

   public String get() {

	String tempLog;

      myLock.lock(true);
	tempLog = log.toString();     
      myLock.lock(false);

      return tempLog;
   }
   
   public void post(String entry) {
      
	long	myTime;

	myLock.lock(true);

   	Date	 time = new Date(); 

	myTime = getTiming();
	log.append( myTime + " : " + entry + "\n");

      myLock.lock(false);
   }     

   public void clear() {

      myLock.lock(true);
	log.setLength(0);
      myLock.lock(false);

   }

   public void startTiming() {

	Date	 time = new Date(); 
	start = time.getTime();	
   }

   public void stopTiming() {
	start = 0;
   }
   
   public void pauseTiming() {
   	Date	 time = new Date(); 
	pause = time.getTime();	
   }

   public void resumeTiming() {
   	Date	 time = new Date(); 
	start = time.getTime() - (pause-start);
   }


   public long getTiming() {
   	Date	 time = new Date();
	long  current;
	if (start > 0)
	   current = time.getTime() - start;
	else
  	   current = 0;
	return current;
   }

   public long getRaw() {
   	Date	 time = new Date();
	long  current;
      current = time.getTime();
	return current;
   }

}
