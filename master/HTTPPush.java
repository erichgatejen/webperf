import java.io.*;
import java.net.*;
import java.util.*;

public class HTTPPush extends Thread {
   
   MCommandQueue		myQueue;
   MCommand			currentCommand;
   MEvent			responseEvent;
   MEventQueue		eventQueue;

   SessionLog		myLog;	// for timing
   public 			boolean   idle  = true;

   URL			targetURL;
   String			targetURLText;
   int			myID;

   String[]			cmdNameTable = { "null", "new", "stop", "pause", "target",
						     "getlog", "go", "ping" };

   private final static String	wordError = "!ERROR!";
   private final static String	wordSucc  = "SUCCESS";
   private final static String	noteFrame = "PRE>";

   // Use default constructor

   public void init(MCommandQueue yourQueue, SessionLog  yourLog,
			  MEventQueue   myEventQueue, int      yourID) {
      setDaemon(true);
	myQueue = yourQueue;
	myLog   = yourLog;
	eventQueue = myEventQueue;
	myID 	  = yourID;
   }

   public void run() {

	Hashtable	info, headers;
	String	response;

	int 		left, right;
	
	headers = new Hashtable();
	headers.put("User-Agent", "JF Perf Tool Master");

	// Loop until killed by the world
	while(true) {
		if (myQueue.has() == true)
		{
			idle   	   = false;
			currentCommand = myQueue.get();

			responseEvent =  new MEvent();
			responseEvent.event = MEvent.eventSLAVE_RESPONSE;
			responseEvent.index = currentCommand.command;
			responseEvent.id    = myID;

			switch(currentCommand.command) {
			
			   case MCommand.cmdNEW:
			   case MCommand.cmdSTOP:
			   case MCommand.cmdPAUSE:
			   case MCommand.cmdTARGET:
			   case MCommand.cmdGETLOG:
			   case MCommand.cmdGO:
			   case MCommand.cmdPING:
// System.out.println("slave command : " + currentCommand.command + "\n");
			       info = new Hashtable();
				 info.put("command", cmdNameTable[currentCommand.command]);
				 info.put("integer", Integer.toString(currentCommand.value));
				 info.put("long",    Long.toString(currentCommand.lvalue));
				 info.put("string",  currentCommand.item);
	 
				 try {
				    targetURL = new URL(targetURLText);
				    response = CGIpost.post(targetURL, headers, info);

				    // See if it worked of didn't
				    if (response.indexOf(wordError) == -1) {

				       // kinda ass-backwards logic here.  *shrug*
					 // this case means things are bad
					 responseEvent.value = MEvent.rSUCCESS;

				    } else {
					 responseEvent.value = MEvent.rFAIL;
			          }

//System.out.println("slave response : " + response + "\n");

				    // Pull out the return note
				    left  = response.indexOf(noteFrame);
				    left  = left + 5;
				    right = response.lastIndexOf(noteFrame);
				    right = right - 1;
				    responseEvent.item = response.substring(left, right);
				    	    			    
				 } catch (Exception e) {
				    // something bad happened.
				    responseEvent.value = MEvent.rIO_ERROR;
				    responseEvent.item  = new String("IO_ERROR");
// System.out.println("Exception !!!!  rIOERROR\n" + e.getMessage() + "\n" + e.toString() + "\n");
				 }
				 break;
 			
			   case MCommand.cmdSET_SLAVE_TARGET:
			      try {
				   targetURLText = new String("http://" + 
							     (String)currentCommand.item +
							     ":8050/");
				} catch (Exception e) {
				   // I dont care
				}
				break;

			   case MCommand.cmdNULL:
			   default:
				responseEvent.value = MEvent.rNULL;
				break;

			}
			eventQueue.put(responseEvent);
			// okies.  gotta sleep for a sec and let target relax.
			try {
				this.sleep(400);
			} catch (InterruptedException e) {
				// Like I give a rats-ass about this lame exception...
			}

		} else {
			idle = true;
			try {
				this.sleep(900);
			} catch (InterruptedException e) {
				// Like I give a rats-ass about this lame exception...
			}

		} // end if
		
	} // end while	

   } // end run()

}     

