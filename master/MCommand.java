import java.util.*;

public class MCommand extends Object {
  
   public static final int cmdNULL        = 0;
   public static final int cmdNEW         = 1;
   public static final int cmdSTOP        = 2;
   public static final int cmdPAUSE       = 3;
   public static final int cmdTARGET      = 4;
   public static final int cmdGETLOG      = 5;
   public static final int cmdGO          = 6;
   public static final int cmdPING        = 7;

   public static final int cmdSET_SLAVE_TARGET        = 10;

   public int  		command; 
   public int    		value;
   public long    	lvalue;
   public Object		item;

}

