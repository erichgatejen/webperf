import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class MasterUI implements ActionListener {

   // Public constants
   public final static int	numberOfSlaves	= 10;

   public final static int	statusGray		= 0;
   public final static int	statusRed		= 1;
   public final static int	statusOrange	= 2;
   public final static int	statusGreen		= 3;
    
   // Private var members ------------------------------------------------------ 

   // Private UI geometry constants
   private final static int	sTextSize		= 50;
   private final static int	sTextSizeStatus	= 15;
   private final static int	sTimeSize		= 12;
   private final static int	sFrameSizeX		= 700;
   private final static int	sFrameSizeY		= 758;
   private final static int	sListGap		= 0;

   // Private vars
   Color cRed;
   Color cGreen;
   Color cGray;
   Color cOrange;

   MEventQueue	myEvents;

   // UI Elements
   Frame	   f;

   Panel	   slaveListPanel1[];
   Label	   slaveListLabel1[];
   TextField   slaveListStatus[];
   TextField   slaveListTargets[];
   TextField   slaveListUser[];
   Panel	   slaveListPanel2[];
   Checkbox	   slaveListEnable2[];
   TextField   slaveListScripts[];


   TextArea    screenLog;
   Panel	   panelButton;

   Panel       top;
   Panel	   list;
   Panel	   bottom;

   TextField   topStatus;
   TextField   topTime;

   Button      bStart;
   Button      bStop;
   Button      bRefresh;
   Button      bPause;
   TextField   testMachine;

   private Color[]  colorStatusTable;

 
   MasterUI(Frame f) {
   
	initialize(f);
   }
   
   private void initialize(Frame f) {  

	// create colors for the UI components
	Color cList1 = new Color(186, 210, 214);
      Color cList2 = new Color(239, 228, 238);

	colorStatusTable = new Color[statusGreen+1];
      colorStatusTable[statusRed]     = new Color(221, 164, 155);
      colorStatusTable[statusGreen]   = new Color(203, 225, 198);
      colorStatusTable[statusGray]    = new Color(225, 226, 224);
      colorStatusTable[statusOrange]  = new Color(253, 235, 208);
	
	this.f = f;
	f.setLayout(new BorderLayout());
	f.setTitle("JF Performance Test Tool - MASTER");

	// Build main Panels
	top    = new Panel();
	top.setLayout(new BorderLayout());

	list   = new Panel();
	GridLayout listLayout = new GridLayout(20,1);
	listLayout.setVgap(sListGap);
	list.setLayout(listLayout);

	bottom = new Panel();
	bottom.setLayout(new BorderLayout());
	
	// Build top and add to top panel
	topStatus = new TextField("Started!");
	topStatus.setEditable(false);
	topTime   = new TextField("0", sTimeSize);
	topTime.setEditable(false);
	top.add(topStatus, "Center");
	top.add(topTime, "East");

	// Build slave lists and add to list panel
	int i;
	slaveListPanel1 = new Panel[numberOfSlaves];
   	slaveListLabel1 = new Label[numberOfSlaves];
	slaveListStatus = new TextField[numberOfSlaves];
	slaveListTargets = new TextField[numberOfSlaves];
	slaveListPanel2  = new Panel[numberOfSlaves];
	slaveListEnable2 = new Checkbox[numberOfSlaves];
      slaveListScripts = new TextField[numberOfSlaves];
      slaveListUser    = new TextField[numberOfSlaves];

	boolean cT = false;

	for (i = 0; i < numberOfSlaves; i++) {

	   slaveListPanel1[i]  = new Panel();
	   slaveListPanel1[i].setLayout(new FlowLayout(FlowLayout.LEFT));
	   slaveListLabel1[i]  = new Label(" " + i + " ");
	   slaveListStatus[i]  = new TextField("NONE", sTextSizeStatus);
	   slaveListStatus[i].setBackground(cGray);
	   slaveListStatus[i].setEditable(false);
	   slaveListTargets[i] = new TextField("no target", sTextSize);
	   slaveListUser[i]    = new TextField("1", 8);
	   slaveListPanel1[i].add(slaveListLabel1[i]);
	   slaveListPanel1[i].add(slaveListStatus[i]);
	   slaveListPanel1[i].add(slaveListTargets[i]);
	   slaveListPanel1[i].add(slaveListUser[i]);
	   
	   slaveListPanel2[i]  = new Panel();
	   slaveListPanel2[i].setLayout (new FlowLayout(FlowLayout.LEFT));
	   slaveListPanel1[i].setBackground(Color.green);
	   slaveListEnable2[i]  = new Checkbox("enable                              .", false);
	   slaveListScripts[i] = new TextField("no script", sTextSize);
	   slaveListPanel2[i].add(slaveListEnable2[i]);
	   slaveListPanel2[i].add(slaveListScripts[i]);

	   if ( cT == true ) {
	      slaveListPanel1[i].setBackground(cList1);
	      slaveListPanel2[i].setBackground(cList1);
		cT = false;
	   } else {
	      slaveListPanel1[i].setBackground(cList2);
	      slaveListPanel2[i].setBackground(cList2);
		cT = true;
	   }

	   list.add(slaveListPanel1[i]);
	   list.add(slaveListPanel2[i]);

	}

	// Build bottom 
	panelButton =  new Panel();
	bStart   = new Button("Start");
 	bStop    = new Button("Stop");
	bRefresh = new Button("Refresh");
	bPause   = new Button("Pause");
      testMachine = new TextField("Test machine", 20);

	panelButton.setLayout(new FlowLayout(FlowLayout.LEFT));
	panelButton.add(bStart);
	panelButton.add(bStop);
	panelButton.add(bRefresh);
	panelButton.add(bPause);
	panelButton.add(testMachine);
	bStart.addActionListener(this);
	bStop.addActionListener(this);
	bRefresh.addActionListener(this);
	bPause.addActionListener(this);
	   
	screenLog = new TextArea(6, 80);
	screenLog.setEditable(false);

	bottom.add(screenLog, "Center");
	bottom.add(panelButton, "South");

	// Complete the frame
     	f.add(top, "North");
	f.add(list, "Center");
	f.add(bottom, "South");
	
	f.setSize(sFrameSizeX,sFrameSizeY);
	f.show();
	      
   }
   
   public void actionPerformed(ActionEvent  e) {
 
	Object  source = e.getSource();
	String  ac	   = e.getActionCommand();

	if ( source == bStart) {
	   pushEvent(MEvent.eventUI_START, 0);   

	} else if ( source == bStop) {
	   pushEvent(MEvent.eventUI_STOP, 0);  
 
	} else if ( source == bRefresh) {
	   pushEvent(MEvent.eventUI_REFRESH, 0);  
 
	} else if ( source == bPause) {
	   pushEvent(MEvent.eventUI_PAUSE, 0);  

	} else {
	   System.out.println("Software Detected Fault : MasterUI.actionPerformed.  Unknown event source.\n");
	   System.out.println("---> ac = " + ac + "\n");
	}
   }

   private void pushEvent(int  eventCode,  int  index) {

	MEvent  anEvent = new MEvent();

	anEvent.event = eventCode;
	anEvent.value = index;

	myEvents.put(anEvent);

   }


   // -----  PUBLIC API ----------------------------------------------

   public void putScreenLog(String entry) {
	screenLog.append(entry + "\n");
   }

   public void setStatus(String text, int	 color) {
	topStatus.setBackground(colorStatusTable[color]);
	topStatus.setText(text);
   }

   public void setSlaveStatus(String text, int	 color, int  slot) {
	slaveListStatus[slot].setBackground(colorStatusTable[color]);
	slaveListStatus[slot].setText(text);
   }
 
   public void clearTarget(int  slot) {
	slaveListTargets[slot].setText("no target");
   }
     
   public String getTarget(int  slot) {
	return slaveListTargets[slot].getText();
   }

   public int getUser(int  slot) {
	int n;
	try {
	   n= Integer.parseInt(slaveListUser[slot].getText());
	   return n;
	} catch (Exception e) {return 0;}
   }

   public void clearScript(int  slot) {
	slaveListScripts[slot].setText("no script");
   }
     
   public String getScript(int  slot) {
	return slaveListScripts[slot].getText();
   }

   public String getTestMachine() {
	return testMachine.getText();
   }

   public boolean isEnabled(int  slot) {
	return slaveListEnable2[slot].getState();
   }

   public void register(MEventQueue  aQueue) {
	myEvents = aQueue;
   }

}
